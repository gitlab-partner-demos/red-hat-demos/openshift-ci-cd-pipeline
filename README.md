### GitOps for GitLab on Openshift

This project leverages a custom GitLab CI/CD flow to deploy a Node Express template application image to a target Openshift environment.

You will need to input the following CI/CD variables into the GitLab project to connect to your Openshift cluster:

- `$OC_URL` = API url of the Openshift instance you are attempting to connect to
- `$OC_TOKEN` =  *masked* personal or service account token of the account you are authenticating from
- `$OC_NAMESPACESLUG` = the base name of the projects you will be deploying your build config, image stream and deployment config to. You will need to create these projects using `oc create project $OC_NAMESPACESLUG-(dev, staging, prod)` to create the relevant projects/namespaces beforehand, or you can include this as an optional step in the GitLab CI file by adding a prebuild stage to verify whether these namespaces/projects exist and creating them if they do not already appear on the cluster.


Demo Setup:

Matt to provide suggestions
1. Set up ROSA or your favorite distribution of Opneshift
2. Set up an auth provider for Openshift, and provide your user with image pull privileges.
3. ...

Demo Flow: 
1. Show Repo (Application + Openshift Manifests in /.Openshift)
2. Show Pipeline
3. Show Pipeline Execution
4. Show Resources in Openshift: BuildConfig, Image Stream, Deployment Config, in node-app-dev and node-app-staging/prod 